function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

let resultado = document.createElement("h2")
document.body.appendChild(resultado);
resultado.style.textAlign = "center";

let voce = document.getElementById("voce");
let computador = document.getElementById("computador");
voce.textContent = 0;
computador.textContent = 0;

// let pedra = 1;
// let papel = 2;
// let tesoura = 3;

function pedraButton() {
    let n1 = getRandomIntInclusive(1, 3);
    // console.log(n1);
    if (n1 == 1) {
        resultado.textContent = "O computador escolheu pedra - DEU EMPATE!";
    }

    if (n1 == 2) {
        resultado.textContent = "O computador escolheu papel - VOCÊ PERDEU!";
        computador.textContent = parseInt(computador.textContent) + 1;
    }

    if (n1 == 3) {
        resultado.textContent = "O computador escolheu tesoura - VOCÊ GANHOU!";
        voce.textContent = parseInt(voce.textContent) + 1;
    }
}

function papelButton() {
    let n1 = getRandomIntInclusive(1, 3);
    // console.log(n1);
    if (n1 == 1) {
        resultado.textContent = "O computador escolheu pedra - VOCÊ GANHOU!";
        voce.textContent = parseInt(voce.textContent) + 1;
    }
    if (n1 == 2) {
        resultado.textContent = "O computador escolheu papel - DEU EMPATE!";
    }
    if (n1 == 3) {
        resultado.textContent = "O computador escolheu tesoura - VOCÊ PERDEU!";
        computador.textContent = parseInt(computador.textContent) + 1;
    }
}

function tesouraButton() {
    let n1 = getRandomIntInclusive(1, 3);
    // console.log(n1);
    if (n1 == 1) {
        resultado.textContent = "O computador escolheu pedra - VOCÊ PERDEU!";
        computador.textContent = parseInt(computador.textContent) + 1;
    }
    if (n1 == 2) {
        resultado.textContent = "O computador escolheu papel - VOCÊ GANHOU!";
        voce.textContent = parseInt(voce.textContent) + 1;
    }
    if (n1 == 3) {
        resultado.textContent = "O computador escolheu tesoura - DEU EMPATE!";
    }
}

